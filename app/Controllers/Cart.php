<?php

namespace Controllers;
use Storage\Base as StorageBase;
use Storage\CartStorage as CartStorage;
use Views\PageBilder;

class Cart extends Base {

	public function start() {
		$db_config = $this->config->get('db');
		$db = new \SafeMySQL($db_config);
		//$StorageBase = new StorageBase($db);
		//$CartStorage = new CartStorage($db);
		$twig_content = [];
		$routeTitle = $this->config->get('routeTitle');
		$twig_content['site'] = $this->getTitle(['title' => $routeTitle[$this->params[0]]], []);
		$twig_content['page'] =  $this->params[0];
		$PageBilder = new PageBilder($this->config);
		$PageBilder->createHtml('Custom\cart', $twig_content);
	}
}