<?php

namespace Controllers;
use Logic\ItemsLists;
use Storage\StoregeItems as StorageItems;
use Storage\StorageCategory as StorageCategory;
use Storage\StorageTags as StorageTags;
use SafeMySQL;
class Base {
	public $params;
	public $subParams;
	public $config;

	public function __construct($params,$subParams, $config) {
		$this->params = $params;
		$this->subParams = $subParams;
		$this->config = $config;
	}
	public function start() {

	}

	/**
	 * @param SafeMySQL $db
	 * @param array $thisCat
	 * @param integer $count
	 *
	 * @return array
	 */
	public function getList($db, $thisCat, $count) {
		$arr = [
			'catId' => $thisCat['id'],
			'page' => 1,
			'count_in_page' => $count
		];
		if(isset($this->subParams['get']['p'])) {
			$arr['page'] = $this->subParams['get']['p'];
		};
		if(isset($this->subParams['get']['filter'])) {
			$arr['filter'] = $this->subParams['get']['filter'];
		};
		if(isset($this->subParams['get']['search'])) {
			$arr['search'] = $this->subParams['get']['search'];
		};
		$storageItems = new StorageItems($db);
		$storageCategory = new StorageCategory($db);
		$storageTags = new StorageTags($db);

		$itemList = new ItemsLists($db);
		$arrList = $itemList->get($arr, $storageItems,$storageCategory, $storageTags);
		return $arrList;
	}

	/**
	 * @param array $catArr
	 * @param array $singleArr
	 *
	 * @return array
	 */
	public function getTitle($catArr, $singleArr) {
		$title['site_name'] = $this->config->get('title');
		$title['description'] = $this->config->get('description');
		$title['keywords'] = $this->config->get('keywords');
		$title['title'] = $this->config->get('title');
		if(isset($catArr['title']) AND !isset($singleArr['title'])) {
			$title['title'] = $catArr['title'].' | '.$this->config->get('title');
		};
		if(isset($singleArr['title'])) {
			$title['title'] = $catArr['site_title'].' '.$singleArr['title'].' | '.$catArr['title'].' | '.$this->config->get('title');
		};
		return $title;
	}

	public function getFilterArr($db) {
		$storageTags = new StorageTags($db);
		$tags = $storageTags->getTags();
		$f_arr = [];
		if(isset($this->subParams['get']['filter'])) {
			$f_arr = explode( ':', $this->subParams['get']['filter'] );
			$f_arr = array_diff( $f_arr, array( '' ) );
		};
		$returnTags = [];
		foreach ($tags as $oneTag) {
			$oneTag['status'] = false;
			if (in_array($oneTag['id'], $f_arr)) {
				$oneTag['status'] = true;
			};
			$returnTags[] = $oneTag;
		}
		return $returnTags;
	}
}