<?php
namespace Controllers;
use Storage\StorageCategory as StorageCategory;
use Storage\StoregeItems as StoregeItems;
use Storage\StorageTags as StoregeTags;
use Views\PageBilder;

class Night_lights extends Base {

	public function start() {
		$db_config = $this->config->get('db');
		$db = new \SafeMySQL($db_config);
		$NightClass = new StorageCategory($db);
		$StoregeTags = new StoregeTags($db);
		$twig_content = [];
		$category_arr = $NightClass->getCategoryByUrl($this->params[0]);
		$twig_content['category'] = $category_arr;
		$twig_content['page'] =  $this->params[0];
		unset($this->params[0]);
		if(count($this->params)>0) {
			$page_type = 'Items\\'.$category_arr['url'];

			$StoregeItems = new StoregeItems($db);
			$arrList = $StoregeItems->getItemByUrl($this->params[1]);
			$twig_content['site'] = $this->getTitle($category_arr, $arrList);
			$twig_content['item'] = $arrList;
		} else {
			$page_type = 'list';
			$twig_content['items'] = $this->getList($db, $category_arr, 12);
			$twig_content['site'] = $this->getTitle($category_arr, []);
			$twig_content['tags'] = $this->getFilterArr($db);
		};
		$PageBilder = new PageBilder($this->config);
		$PageBilder->createHtml($page_type, $twig_content);
	}
}