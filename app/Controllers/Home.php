<?php

namespace Controllers;
use Views\PageBilder;

class Home extends Base {

	public function start() {
		$twig_content = [];
		$routeTitle = $this->config->get('routeTitle');
		$twig_content['site'] = $this->getTitle(['title' => $routeTitle[$this->params[0]]], []);
		$twig_content['page'] =  $this->params[0];
		$PageBilder = new PageBilder($this->config);
		$PageBilder->createHtml('Custom\home', $twig_content);
	}
}