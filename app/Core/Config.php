<?php

class Config {
	private $arr;
	public function __construct() {
		if (is_readable(ROOT.'/app/config.php')) {
			$this->arr = require(ROOT.'/app/config.php');
		}

	}
	public function get($type) {
		if(isset($this->arr[$type])) {
			return $this->arr[$type];
		}
	}
}