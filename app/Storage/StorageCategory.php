<?php
/**
 * Created by PhpStorm.
 * User: kemtovez
 * Date: 22.09.2019
 * Time: 12:43
 */

namespace Storage;
use SafeMySQL;

class StorageCategory {
	protected $db;
	public function __construct(SafeMySQL $db)
	{
		$this->db = $db;
	}

	/**
	 * @return array
	 */
	public function getCategorys() {
		$data = $this->db->getAll("SELECT * FROM categorys WHERE `active`='1' ORDER BY `num` ASC");
		return $data;
	}

	/**
	 * @param string $url
	 *
	 * @return array|FALSE
	 */
	public function getCategoryByUrl($url) {
		$data = $this->db->getRow("SELECT * FROM categorys WHERE `active`='1' AND `url`= ?s", $url);
		return $data;
	}

	/**
	 * @param integer $id
	 *
	 * @return array
	 */
	public function getParamsByCatId($id) {
		$data = $this->db->getAll("SELECT * FROM items_params WHERE `id_categorys`= ?i AND `active`='1' ORDER BY `price` ASC", $id);
		return $data;
	}
}