<?php
/**
 * Created by PhpStorm.
 * User: kemtovez
 * Date: 22.09.2019
 * Time: 12:44
 */

namespace Storage;
use SafeMySQL;

class StorageTags {
	protected $db;
	public function __construct(SafeMySQL $db)
	{
		$this->db = $db;
	}

	/**
	 * @return array
	 */
	public function getTags() {
		$arr = [];
		$data = $this->db->getAll("SELECT * FROM tags");
		foreach ($data as $one_tag) {
			$arr[$one_tag['id']] = $one_tag;
		}
		return $arr;
	}

	/**
	 * @param array $idItems
	 *
	 * @return array
	 */
	public function getItemsByTags($idItems) {
		$arr = [];
		$data = $this->db->getAll("SELECT * FROM items_tags WHERE `id_item` IN (?a)", $idItems);
		foreach ($data as $one_tag) {
			$arr[$one_tag['id_item']][] = $one_tag['id_tag'];
		}
		return $arr;
	}
}