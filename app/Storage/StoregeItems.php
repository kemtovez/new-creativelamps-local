<?php
/**
 * Created by PhpStorm.
 * User: kemtovez
 * Date: 22.09.2019
 * Time: 12:39
 */

namespace Storage;
use SafeMySQL;

class StoregeItems {
	protected $db;
	public function __construct(SafeMySQL $db)
	{
		$this->db = $db;
	}

	/**
	 * @param integer $page
	 * @param integer $limit
	 * @param integer $idCat
	 * @param array $ids
	 * @param string $search
	 *
	 * @return array
	 */
	public function getItemsList($page,$limit,$idCat, $ids, $search) {
		$ot = $page * $limit - $limit;
		if(count($ids)>0) {
			$data = $this->db->getAll("SELECT * FROM items WHERE `id_categorys`= ?i AND `title` LIKE '%$search%' AND `id` IN (?a) AND `public`='1' ORDER BY `id` DESC LIMIT ?i, ?i",$idCat, $ids, $ot, $limit);
		} else {
			$data = $this->db->getAll("SELECT * FROM items WHERE `id_categorys`= ?i AND `title` LIKE '%$search%' AND `public`='1' ORDER BY `id` DESC LIMIT ?i, ?i", $idCat, $ot, $limit);
		}
		return $data;
	}

	/**
	 * @param string $url
	 *
	 * @return array|FALSE
	 */
	public function getItemByUrl($url) {
		$data = $this->db->getRow("SELECT * FROM items WHERE `url`= ?s AND `public`='1'", $url);
		return $data;
	}

	/**
	 * @param array $arrTags
	 *
	 * @return array|FALSE
	 */
	public function getItemsTags($arrTags) {
		$data = $this->db->getCol( "SELECT `id_item` FROM `items_tags` WHERE `id_tag` IN (?a) GROUP BY `id_item`", $arrTags);
		return $data;
	}
}