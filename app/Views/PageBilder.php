<?php
/**
 * Created by PhpStorm.
 * User: kemtovez
 * Date: 15.09.2019
 * Time: 12:30
 */

namespace Views;


class PageBilder {
	protected $config;
	public function __construct($config) {
		$this->config = $config;
	}

	/**
	 * @param string $type
	 * @param array $twig_content
	 *
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function createHtml($type, $twig_content) {
		$path = $this->config->get('template');
		$loader = new \Twig\Loader\FilesystemLoader($path);
		$twig = new \Twig\Environment($loader);
		echo $twig->render($type.'.twig', $twig_content);
	}
}