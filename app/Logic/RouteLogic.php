<?php

namespace Logic;
use Config;

class RouteLogic {
	protected $config;
	public function __construct(Config $config) {
		$this->config = $config;
	}

	public function delegate() {
		$queryParams = $this->urlDecode();
		$querySubParams = $this->getSubParams();
		$settingRoute = $this->config->get('route');

		if(isset($queryParams[0])) {
			if (in_array($queryParams[0], $settingRoute)) {
				$contName = $queryParams[0];
			} else {
				$contName = 'home';
			}
			$indexController = 'Controllers' . '\\' . ucfirst( $contName );
			if (class_exists($indexController)) {
				$controller  = new $indexController( $queryParams, $querySubParams,  $this->config);
				$controller -> start();
			}
		}
	}

	/**
	 * @return array
	 */
	private function getSubParams() {
		$subParams['get'] = $this->parse($_GET);
		$subParams['post'] = $this->parse($_POST);
		$subParams['input'] = [];
		$input = json_decode( file_get_contents( 'php://input' ), true );
		if (json_last_error() === 0) {
			$subParams['input'] = $this->parse($input);
		};
		return $subParams;
	}

	/**
	 * @param array $arr
	 *
	 * @return array
	 */
	private function parse(array $arr) {
		$subParams = [];
		if(count($arr)>0) {
			if(isset($arr['q'])) {
				unset($arr['q']);
			}
			foreach ($arr as $key=>$val) {
				if(is_array($arr[$key])) {
					$val = $arr[ $key ] ;
				} else {
					$val = $arr[ $key ];
				}
				$subParams[$key] = $val;
			}
		};
		return $subParams;
	}

	/**
	 * @return array
	 */
	private function urlDecode() {
		$params = [];
		$queryString = str_replace("q=","",trim($_SERVER['REQUEST_URI']));
		$queryParams = explode('/', urldecode($queryString));
		$queryParams = array_diff($queryParams, array(''));
		if(count($queryParams)==0) {
			$params[] = 'home';
		}
		foreach ($queryParams as $lineUrl) {
			$getString = explode('?', $lineUrl);
			$getString = array_diff($getString, array(''));
			if(isset($getString[0])) {
				$lineUrl = $getString[0];
				preg_match_all("%[a-zA-Z0-9_-]%", $lineUrl, $m, PREG_SET_ORDER);
				if (count($m)) {
					$params[] = $lineUrl;
				}
			}
		};
		return $params;
	}
}