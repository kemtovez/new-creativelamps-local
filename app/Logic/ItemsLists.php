<?php

namespace Logic;
use Storage\StorageCategory as StorageCategory;
use Storage\StorageTags as StorageTags;
use Storage\StoregeItems as StorageItems;

class ItemsLists {
	protected $db;
	public function __construct($db) {
		$this->db = $db;
	}

	/**
	 * @param $arr
	 * @param StorageItems $Storage
	 * @param StorageCategory $StorageCategory
	 * @param StorageTags $StorageTags
	 *
	 * @return array
	 */
	public function get($arr, StorageItems $Storage, StorageCategory $StorageCategory, StorageTags $StorageTags) {
		$search = '';
		if(isset($arr['search'])) {
			$search = $arr['search'];
		};
		$arrItemsId = [];
		if(isset($arr['filter'])) {
			$f_arr = explode(':', $arr['filter']);
			$f_arr = array_diff( $f_arr, array( '' ) );
			$arrItemsId = $Storage->getItemsTags($f_arr);
		};
		$list = $Storage->getItemsList($arr['page'],$arr['count_in_page'],$arr['catId'], $arrItemsId, $search);
		$list = $this->createPrice($list, $arr['catId'], $StorageCategory, 'low');
		$list = $this->addTags($list, $StorageTags);
		return $list;
	}

	/**
	 * @param array $list
	 * @param integer $catId
	 * @param StorageCategory $Storage
	 * @param string $type
	 *
	 * @return array
	 */
	private function createPrice(array $list, $catId, StorageCategory $Storage, $type) {
		$paramsPrice = $Storage->getParamsByCatId($catId);
		$basePrice = 0;
		if(isset($paramsPrice[0]['price'])) {
			$basePrice = $paramsPrice[0]['price'];
			if($type=='low') {
				$basePrice = $paramsPrice[0]['price'];
			} else if($type=='high') {
				$basePrice = end($paramsPrice)['price'];
			};
		};
		$priceArr = [];
		foreach ($list as $one) {
			$one['price'] = (($basePrice / 100) * $one['sale']) + $basePrice;
			$priceArr[] = $one;
		};
		return $priceArr;
	}

	/**
	 * @param array $listArr
	 * @param StorageTags $StorageTags
	 *
	 * @return array
	 */
	private function addTags(array $listArr, StorageTags $StorageTags) {
		$itemsId = [];
		foreach ($listArr as $one) {
			$itemsId[] = $one['id'];
		}
		$tags = $StorageTags->getTags();
		$itemsTags = $StorageTags->getItemsByTags($itemsId);

		$list = [];
		foreach ($listArr as $one) {
			$id = $one['id'];
			if(isset($itemsTags[$id])){
				$itemT = [];
				foreach ($itemsTags[$id] as $one_tag) {
					$itemT[] = array(
						'id' => $one_tag,
						'name' => $tags[$one_tag]['name'],
						'url' => $tags[$one_tag]['url']
					);
				}
				$one['tags'] = $itemT;
			};
			$list[] = $one;
		};
		return $list;
	}

}