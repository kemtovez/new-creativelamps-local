<?php
return array(
	'db' => array(
		'host' => 'localhost',
		'user' => 'root',
		'pass' => '',
		'db' => 'creative2'
	),
	'route' => array('home', 'night_lights', 'framework', 'matrix', 'crystals', 'clocks', 'monoblock', 'plates', 'others', 'page', 'cart', 'profile'),
	'list' => array(
		'count_in_page' => 12
	),
	'template'   => ROOT . '/app/Twig/',
	'title' => 'Creative Lamps',
	'description' => 'Не можете уснут в темноте, но и при включенном свете спать не комфортно? Устали ударятся мизинцем об угол любимой тумбочки или наступать на детские игрушки в ночной темноте? Хотите сделать необычный подарок своим близким или коллегам и не знаете что подарить? Не знаете какой “вишенкой на торте” дополнить дизайн своей спальни, детской комнаты или ресепшена …',
	'keywords'    => 'ночник, подарок',
	'routeTitle' => array(
		'home' => 'Главная',
		'night_lights' => 'Ночники',
		'framework' => '',
		'matrix' => '',
		'crystals' => '',
		'clocks' => '',
		'monoblock' => '',
		'plates' => 'Сменные пластины',
		'others' => 'Разное',
		'page' => 'Страница',
		'cart' => 'Корзина',
		'profile' => 'Профиль'
	)
);