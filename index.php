<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);
date_default_timezone_set('UTC');


define('ROOT', __DIR__);
require ROOT . '/vendor/autoload.php';

$config = new Config();
$route = new \Logic\RouteLogic($config);
$route->delegate();

?>
