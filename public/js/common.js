jQuery(document).ready(function ($)  {
    function ajax_json($results) {
        if ($results.error) {
            $error_text = '';
            $.each($results.error, function() {
                $error_text = $error_text + this+'. ';
            });
            swal ( "Oops" ,  $error_text ,  "error" );
        } else {
            $.each($results.result, function() {
                if (this.status == 'reload') {
                    location.reload();
                };
                if (this.status == 'go_to') {
                    window.location.href = this.text;
                };
                if (this.status == 'hide_block') {
                    $('.'+this.text).hide();
                };
                if (this.status == 'show_block') {
                    $('.'+this.text).show();
                };
                if (this.status == 'insert_to_block') {
                    $('#'+this.id_to_block).val(this.value);
                };
                if (this.status == 'comment') {
                    swal("Good job!", this.text, "success");
                };
            });
        }
    };

    $(".ajax_form").submit(function(e){
        e.preventDefault();
        var m_method=$(this).attr("method");
        var m_action=$(this).attr("action");
        var m_data=$(this).serialize();
        $.ajax({
            type: m_method,
            url: m_action,
                dataType: 'json',
            data: m_data,
            success: function (results) {
                console.log(results);
                ajax_json(results);
            }, error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });

    var ctx = document.getElementById("recent-rep2-chart");
    if (ctx) {
        ctx.height = 230;
        $.ajax({
            type: 'POST',
            url: 'controllers/chart1.php',
            dataType: 'json',
            success: function (results) {
                console.log(results);
                    data1 = results.data;
                    data2 = results.data2;
                    data3 = results.data3;
                   // data4 = results.data4;
                    labels = results.labels;

                var myChart = new Chart(ctx, {
                    type: 'line',
                    data: {
                        labels: labels,
                        datasets: [
                            {
                                label: 'Всего',
                                backgroundColor: 'transparent',
                                borderColor: 'rgba(0,173,95,0.9)',
                                pointHoverBackgroundColor: '#fff',
                                borderWidth: 0,
                                data: data1

                            },
                            {
                                label: 'Запломбировано',
                                backgroundColor: 'transparent',
                                borderColor: 'rgba(0,181,233,0.9)',
                                pointHoverBackgroundColor: '#fff',
                                borderWidth: 0,
                                data: data2

                            },
                            {
                                label: 'Распломбировано',
                                backgroundColor: 'transparent',
                                borderColor: 'rgba(233, 123, 0, 0.9)',
                                pointHoverBackgroundColor: '#fff',
                                borderWidth: 0,
                                data: data3

                            }
                        ]
                    },
                    options: {
                        maintainAspectRatio: true,
                        legend: {
                            display: false
                        },
                        responsive: true,
                        scales: {
                            xAxes: [{
                                gridLines: {
                                    drawOnChartArea: true,
                                    color: '#f2f2f2'
                                },
                                ticks: {
                                    fontFamily: "Poppins",
                                    fontSize: 12
                                }
                            }],
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    maxTicksLimit: 5,
                                    stepSize: 50,
                                    max: 150,
                                    fontFamily: "Poppins",
                                    fontSize: 12
                                },
                                gridLines: {
                                    display: true,
                                    color: '#f2f2f2'

                                }
                            }]
                        },
                        elements: {
                            point: {
                                radius: 0,
                                hitRadius: 10,
                                hoverRadius: 4,
                                hoverBorderWidth: 3
                            },
                            line: {
                                tension: 0
                            }
                        }


                    }
                });
            }, error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    }

    var quill = new Quill('#textarea-input', {
        modules: {
            toolbar: [['bold', 'italic', 'link']]
        },
        placeholder: 'Какой-то текст',
        theme: 'bubble'
    });

    function quillGetHTML(inputDelta) {
        var tempCont = document.createElement("div");
        (new Quill(tempCont)).setContents(inputDelta);
        return tempCont.getElementsByClassName("ql-editor")[0].innerHTML;
    }

function news($type) {
    $data = '';
    $url = '../controllers/add_news.php';
    if($type=='add') {
        $url = 'controllers/add_news.php';
            $img = $('#img').val();
            $lang = $('#lang').val();
            $text = quillGetHTML(quill.getContents());
            var length = quill.getLength();
            if(length>2) {
                $data = 'text=' + $text + '&img=' + $img+'&type='+$type+'&lang='+$lang;
            } else {
                swal ( "Oops" ,  "Текст слишком короткий" ,  "error" );
            }
        };
    if($type=='update') {
        $id = $('#news_id').val();
        $img = $('#img').val();
        $lang = $('#lang').val();
        $text = quillGetHTML(quill.getContents());
        var length = quill.getLength();
        if(length>2) {
            $data = 'id='+$id+'&text=' + $text + '&img=' + $img+'&type='+$type+'&lang='+$lang;
        } else {
            swal ( "Oops" ,  "Текст слишком короткий" ,  "error" );
        }
    };
    if($type=='remove') {
        $id = $('#news_id').val();
        $data = 'id='+$id+'&type='+$type;
    };
    if($data!=''){
        $.ajax({
            type: 'POST',
            url: $url,
            dataType: 'json',
            data: $data,
            success: function (results) {
                console.log(results);
                ajax_json(results);
            }, error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    };
}

    $('.add_new_news').click(function (e) {
        e.preventDefault();
        news('add');
    });

    $('.edit_new_news').click(function (e) {
        e.preventDefault();
        news('update');
    });

    $('.dell_new_news').click(function (e) {
        e.preventDefault();
        news('remove');
    });

    function generate_input() {
        $start_plomb = $('#id_plomb').val();
        $count = parseInt($('#count_plomb').val());
        if($('#count_plomb').val()!='') {
            $num = parseInt($('#id_plomb').val().replace(/\D+/g, ""));
            $char = $('#id_plomb').val().replace(/\d/g, '');
            $next_num = $num + $count - 1;
            $cout_num = $next_num.toString().length;
            $last_count = 8 - $cout_num;
            for (var i=1; i <= $last_count; i += 1){
                $char = $char+'0';
            }
            $('#last_plomb').val($char + $next_num);
        } else {
            $('#last_plomb').val('');
        };
    }
    $("#count_plomb").keyup(function (e) {
        generate_input();
    });
    $("#id_plomb").keyup(function (e) {
        generate_input();
    });


    function TransferCompleteCallback(content){
    }
    var input = document.getElementById("input_files");
    if (input) {
        var formdata = false;
        if (window.FormData) {
            formdata = new FormData();
            $("#btn_submit").hide();
            $("#loading_spinner").hide();
        }
        $('#input_files').on('change',function(event){
            var i = 0, len = this.files.length, img, reader, file;
            //console.log('Number of files to upload: '+len);
            $('#result').html('');
            $('#input_files').prop('disabled',true);
            $("#loading_spinner").show();
            for ( ; i < len; i++ ) {
                file = this.files[i];
                if(!!file.name.match(/.*\.pdf$/)){
                    if ( window.FileReader ) {
                        reader = new FileReader();
                        reader.onloadend = function (e) {
                            TransferCompleteCallback(e.target.result);
                        };
                        reader.readAsDataURL(file);
                    }
                    if (formdata) {
                        formdata.append("files[]", file);
                    }
                } else {
                    $("#loading_spinner").hide();
                    $('#input_files').val('').prop('disabled',false);
                    alert(file.name+' is not a PDF');
                }
            }
            if (formdata) {
                $.ajax({
                    url: "../controllers/upload_pdf.php",
                    type: "POST",
                    data: formdata,
                    processData: false,
                    contentType: false, // this is important!!!
                    success: function (res) {
                        var result = JSON.parse(res);
                        console.log(result);
                        $("#loading_spinner").hide();
                        $('#input_files').val('').prop('disabled',false);
                        if(result.res === true){
                            $('#file_url').val(result.token);
                            $('#pdf_file_upload').hide();
                            var buf = '<ul class="list-group">';
                            for(var x=0; x<result.data.length; x++){
                                buf+='<li class="list-group-item"><a href="../../uploads/expertise/'+result.token+'.pdf">'+result.data[x]+'</a></li>';
                            }
                            buf += '</ul>';
                            $('#result').html('<strong>Files uploaded:</strong>'+buf);
                        } else {
                            $('#result').html(result.data);
                        }
                        // reset formdata
                        formdata = false;
                        formdata = new FormData();
                    }
                });
            }
            return false;
        });
    };

    $('.add_new_expertise').click(function (e) {
        e.preventDefault();
        $id = $('#event_id').val();
        $img = $('#file_url').val();
        $text = $('#textarea-expertise').val();
        if($text!='') {
            $.ajax({
                type: 'POST',
                url: '../controllers/add_expertise.php',
                dataType: 'json',
                data: 'id='+$id+'&token='+$img+'&text='+$text,
                success: function (results) {
                    console.log(results);
                    ajax_json(results);
                }, error: function (xhr, ajaxOptions, thrownError) {
                    console.log(thrownError);
                }
            });
        }
    });
    $('.clear_pdf').click(function (e) {
        e.preventDefault();
        $(this).parent('li').remove();
        $('#file_url').val('');
    });

});